﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEditor;
using Dreamteck.Splines;
using System.IO;
using UnityEditor.Experimental.SceneManagement;


[InitializeOnLoad]
public static class AutoBake
{
    //static AutoBake()
    //{
    //    PrefabUtility.prefabInstanceUpdated += OnPrefabInstanceUpdated;
    //    EditorSceneManager.sceneSaving += OnSceneSaving;
    //    EditorSceneManager.sceneDirtied += OnSceneDirtied;
    //}

    //static void OnSceneSaving(UnityEngine.SceneManagement.Scene scene, string path)
    //{
    //    //MeshGenerator[] meshGens = 
    //    UnityEngine.Debug.LogFormat("Saving scene '{0}'", scene.name);
    //    //foreach (GameObject obj in scene.GetRootGameObjects())
    //    //    foreach (MeshGenerator meshGen in obj.GetComponentsInChildren<MeshGenerator>())
    //    //        CheckBakeState(meshGen);
    //}

    //static void OnPrefabInstanceUpdated(GameObject instance)
    //{
    //    Debug.Log("Prefab update: " + instance);

    //    //foreach (MeshGenerator meshGen in instance.GetComponentsInChildren<MeshGenerator>())
    //    //    CheckBakeState(meshGen);

    //    //EditorSceneManager.SaveScenes(dirtyScenes.ToArray());
    //    //dirtyScenes.Clear();
    //}

    //static List<Scene> dirtyScenes = new List<Scene>();
    //static void OnSceneDirtied(Scene scene)
    //{
    //    //dirtyScenes.Add(scene);
    //}

    [MenuItem("Tools/Bake Scene _%#d")]
    public static void BakeScene()
    {
        Scene scene = EditorSceneManager.GetActiveScene();
        if (PrefabStageUtility.GetCurrentPrefabStage() != null)
        {
            PrefabStage stage = PrefabStageUtility.GetCurrentPrefabStage();
            scene = stage.scene;
            //return; 
        }

        HashSet<string> prefabs = new HashSet<string>();
        //Scene scene = EditorSceneManager.GetActiveScene();
        foreach (GameObject obj in scene.GetRootGameObjects())
            CollectPrefabs(obj, ref prefabs);

        foreach (string prefab in prefabs)
        {
            GameObject root = PrefabUtility.LoadPrefabContents(prefab);
            Debug.LogFormat("rebaking parent prefab {0}", prefab);
            foreach (MeshGenerator meshGen in root.GetComponentsInChildren<MeshGenerator>())
            {
                CheckBakeState(meshGen, true);
            }
            PrefabUtility.SaveAsPrefabAsset(root, prefab);
            PrefabUtility.UnloadPrefabContents(root);
        }

        foreach (GameObject obj in scene.GetRootGameObjects())
            foreach (MeshGenerator meshGen in obj.GetComponentsInChildren<MeshGenerator>())
                CheckBakeState(meshGen, true);
    }

    static void CollectPrefabs(GameObject root, ref HashSet<string> prefabs)
    {
        foreach (MeshGenerator meshGen in root.GetComponentsInChildren<MeshGenerator>())
        {
            string prefabPath = "";
            GameObject prefabInstanceRoot = PrefabUtility.GetOutermostPrefabInstanceRoot(meshGen);
            if (prefabInstanceRoot == null)
                continue;

            GameObject prefabRoot = PrefabUtility.GetCorrespondingObjectFromSource(prefabInstanceRoot);
            if (prefabRoot == null)
                continue;

            prefabPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(prefabRoot);
            if (!string.IsNullOrEmpty(prefabPath))
                prefabs.Add(prefabPath);

            CollectPrefabs(prefabRoot, ref prefabs);
        }
    }

    public static void Bake(MeshGenerator meshGen)
    {
        if (meshGen.baked)
            return;

        string meshName = GetMeshHierarchyName(meshGen);
        string path = "Assets" + GetSavePath(meshGen) + "/" + meshName + ".asset";
        Mesh existingMesh = AssetDatabase.LoadAssetAtPath<Mesh>(path);
        if (existingMesh != null)
            meshGen.mesh = existingMesh;

        meshGen.RebuildImmediate();
        meshGen.Bake(false, false);

        //string meshName = GetMeshHierarchyName(meshGen);
        meshGen.mesh.name = meshName;
        MeshFilter filter = meshGen.GetComponent<MeshFilter>();
        filter.sharedMesh = meshGen.mesh;
        PrefabUtility.RecordPrefabInstancePropertyModifications(filter);
        PrefabUtility.RecordPrefabInstancePropertyModifications(meshGen);

        //string path = "Assets" + GetSavePath(meshGen) + "/" + meshName + ".asset";
        //AssetDatabase.DeleteAsset(path);
        //Mesh existingMesh = AssetDatabase.LoadAssetAtPath<Mesh>(path);
        if (existingMesh == null)
            AssetDatabase.CreateAsset(meshGen.mesh, path);
        else
        {
            //EditorUtility.CopySerializedManagedFieldsOnly(meshGen.mesh, existingMesh);
            //AssetDatabase.
            //AssetDatabase.RemoveObjectFromAsset(existingMesh);
            //existingMesh.Clear();
            //AssetDatabase.AddObjectToAsset(meshGen.mesh, path);
            AssetDatabase.SaveAssets();
        }
        //Debug.Log("after baking mesh " + meshGen.mesh.name);
    }

    public static void DeleteMesh(MeshGenerator meshGen)
    {
        string path = "Assets" + GetSavePath(meshGen) + "/" + GetMeshHierarchyName(meshGen) + ".asset";
        AssetDatabase.DeleteAsset(path);
    }

    public static string GetSavePath(MeshGenerator meshGen, bool createDirectories = true)
    {
        string savePath = "/Models";
        if (!Directory.Exists(Application.dataPath + savePath) && createDirectories)
            Directory.CreateDirectory(Application.dataPath + savePath);

        savePath += "/AutoBake";
        if (!Directory.Exists(Application.dataPath + savePath) && createDirectories)
            Directory.CreateDirectory(Application.dataPath + savePath);

        //if (PrefabStageUtility.GetPrefabStage(meshGen.gameObject) != null)
        //    Debug.LogFormat("prefabstage of baked object: {0}", PrefabStageUtility.GetPrefabStage(meshGen.gameObject).scene.name);
        
        //Debug.LogFormat("scene of baked object: {0}", meshGen.gameObject.scene.name);
        //if (PrefabStageUtility.GetCurrentPrefabStage() != null)
        //{
        //    Debug.LogFormat("curprst: {0}, mgo prst: {1}", PrefabStageUtility.GetCurrentPrefabStage().scene.name, PrefabStageUtility.GetPrefabStage(meshGen.gameObject).scene.name);
            savePath += "/" + meshGen.gameObject.scene.name;
        //}
        //else
        //    savePath += "/" + EditorSceneManager.GetActiveScene().name;

        if (!Directory.Exists(Application.dataPath + savePath) && createDirectories)
            Directory.CreateDirectory(Application.dataPath + savePath);

        return savePath;
    }

    //1) check if overridden points are nearly equal to its prefab value.
    //2) if one spline point is overridden, set all points overridden.
    public static void CheckSplineOverrides(SplineComputer instanceSpline)
    {
        SplineComputer prefabSpline = PrefabUtility.GetCorrespondingObjectFromSource(instanceSpline);
        if (prefabSpline == null)
            return;

        SerializedObject instanceSo = new SerializedObject(instanceSpline);
        SerializedProperty instanceProp = instanceSo.FindProperty("spline.points");
        if (!instanceProp.prefabOverride)
            return;

        bool overrideAll = false;
        int numPoints = instanceSpline.pointCount;
        if (numPoints != prefabSpline.pointCount)
            overrideAll = true;
        else
        {
            for (int i = 0; i < numPoints; ++i)
            {
                if (SplinePoint.AreDifferent(ref instanceSpline.GetSpline().points[i], ref prefabSpline.GetSpline().points[i]))
                {
                    overrideAll = true;
                    break;
                }
            }
        }

        if (overrideAll)
        {
            List<PropertyModification> mods = new List<PropertyModification>( PrefabUtility.GetPropertyModifications(instanceSpline) );
            SerializedProperty sizeProp = instanceProp.FindPropertyRelative("Array.size");

            //while (instanceProp.NextVisible(true))
            //{
            //    Debug.Log(instanceProp.propertyPath);
            //}
            //instanceProp = instanceSo.FindProperty("spline.points");
            mods.Add(GetPropertyModification(sizeProp.propertyPath, sizeProp.intValue.ToString(), prefabSpline));

            for (int i = 0; i < numPoints; ++i)
            {
                SerializedProperty pointProp = instanceProp.GetArrayElementAtIndex(i);
                while (pointProp.Next(pointProp.propertyType != SerializedPropertyType.Color) && pointProp.propertyType != SerializedPropertyType.Generic)
                {
                    //Debug.Log("checking prop " + pointProp.propertyPath + " type " + pointProp.propertyType);
                    if (pointProp.prefabOverride || pointProp.isDefaultOverride || pointProp.propertyType != SerializedPropertyType.Float)
                        continue;
                    //Debug.Log("overriding prop " + pointProp.propertyPath);
                    mods.Add(GetPropertyModification(pointProp.propertyPath, pointProp.floatValue.ToString(), prefabSpline));
                }
                PrefabUtility.SetPropertyModifications(instanceSpline, mods.ToArray());
            }
        }
        else
            while (instanceProp.Next(true))
                PrefabUtility.RevertPropertyOverride(instanceProp, InteractionMode.AutomatedAction);

        PrefabUtility.RecordPrefabInstancePropertyModifications(instanceSpline);
    }

    static PropertyModification GetPropertyModification(string propertyPath, string value, Object target)
    {
        PropertyModification mod = new PropertyModification();
        mod.propertyPath = propertyPath;
        mod.value = value;
        mod.target = target;
        return mod;
    }

    public static void CheckBakeState(MeshGenerator meshGen, bool forceRebake = false)
    {
        //if (!PrefabUtility.IsPartOfPrefabInstance(meshGen) || meshGen.spline.HasOverridesAtPath("spline.points"))
        //    return;

        CheckSplineOverrides(meshGen.spline);
        if (forceRebake)
        {
            meshGen.spline.RebuildImmediate(true, true);
            Unbake(meshGen);
        }

        if (meshGen.baked)
        {
            bool hasOverriddenMesh = meshGen.GetComponent<MeshFilter>().HasOverridesAtPath("m_Mesh");
            bool hasOverriddenSplinepoints = meshGen.spline.HasOverridesAtPath("spline.points");
            if (PrefabUtility.IsPartOfPrefabInstance(meshGen) && hasOverriddenMesh && !hasOverriddenSplinepoints)
                Unbake(meshGen);
            else
                return;
        }

        //meshGen.RevertNearlyEqualOverrides();

        //MeshGenerator prefabComp = PrefabUtility.GetCorrespondingObjectFromSource(meshGen);
        //if (prefabComp != null)
        //    CheckBakeState(prefabComp, forceRebake);

        //if (meshGen.HasOverridesAtPath("mesh") && )
        bool setDirty = false;
        if (!PrefabUtility.IsPartOfPrefabInstance(meshGen) || meshGen.spline.HasOverridesAtPath("spline.points"))
        {
            Bake(meshGen);
            setDirty = true;
        }
        else
        {
            meshGen.Bake(false, false); //needed for internal MeshGenerator states, although all relevant properties are reverted to its parent prefab values afterwards.
            MeshFilter filter = meshGen.GetComponent<MeshFilter>();
            //DeleteMesh(meshGen);

            SerializedObject so = new SerializedObject(meshGen);
            setDirty = RevertIfOverriden(so.FindProperty("_baked")) || setDirty;
            setDirty = RevertIfOverriden(so.FindProperty("mesh")) || setDirty;
            so = new SerializedObject(filter);
            setDirty = RevertIfOverriden(so.FindProperty("m_Mesh")) || setDirty;

            //meshGen.RevertBrokenReferenceOverrides();
            //filter.RevertBrokenReferenceOverrides();
            Debug.LogFormat("mesh after reverting object {0} override {1}", GetMeshHierarchyName(meshGen), meshGen.mesh != null ? meshGen.mesh.name : "no mesh!!");
        }
        if (setDirty)
            EditorUtility.SetDirty(meshGen);

        meshGen.RevertOverridesAtPath("surfaceTris");
        meshGen.RevertOverridesAtPath("projectedVerts");

        //Debug.LogFormat("after check bake {0}, filter name {1}", meshGen.mesh.name, meshGen.GetComponent<MeshFilter>().sharedMesh.name);
    }

    public static void CheckBakeState(SplineComputer spline)
    {
        foreach (MeshGenerator meshGen in spline.GetComponentsInChildren<MeshGenerator>())
            CheckBakeState(meshGen);
    }

    public static bool RevertIfOverriden(SerializedProperty prop)
    {
        if (!prop.prefabOverride)
            return false;

        PrefabUtility.RevertPropertyOverride(prop, InteractionMode.AutomatedAction);
        PrefabUtility.RecordPrefabInstancePropertyModifications(prop.serializedObject.targetObject);
        return true;
    }

    public static void Unbake(MeshGenerator meshGen)
    {
        //Debug.Log("Unbake called on mesh baked " + meshGen.baked);
        if (!meshGen.baked)
            return;

        meshGen.mesh = new Mesh();
        meshGen.mesh.name = "tempMesh";
        meshGen.Unbake();
        PrefabUtility.RecordPrefabInstancePropertyModifications(meshGen);
    }

    public static string GetMeshHierarchyName(Component meshGen)
    {
        Transform t = meshGen.transform;
        string name = t.name;
        while (t.parent != null)
        {
            t = t.parent;
            if (t.name == "Prefab Mode in Context")
                break;
            name = t.name + "-" + name;
        }
        return name;
    }
}


public static class Extensions
{
    public const float epsilon = 0.001f;

    public static bool NearlyEqual(this Vector3 left, Vector3 right)
    {
        return Mathf.Abs(left.x - right.x) < epsilon && Mathf.Abs(left.y - right.y) < epsilon && Mathf.Abs(left.z - right.z) < epsilon;
    }

    public static void RevertNearlyEqualOverrides(this Component instanceComponent)
    {
        //return;

        Component prefabComp = PrefabUtility.GetCorrespondingObjectFromSource(instanceComponent);
        if (prefabComp == null)
            return;
         
        SerializedObject instanceSo = new SerializedObject(instanceComponent);
        SerializedObject prefabSo = new SerializedObject(prefabComp);
        SerializedProperty instanceProp = instanceSo.GetIterator();
        while (instanceProp.Next(true))
        {
            string path = instanceProp.propertyPath;
            SerializedPropertyType type = instanceProp.propertyType;
            if (!instanceProp.prefabOverride || instanceProp.isDefaultOverride || (instanceProp.propertyType != SerializedPropertyType.Float && instanceProp.propertyType != SerializedPropertyType.Integer))
                continue;

            //if (instanceProp.propertyPath.Contains("mesh"))
            //{ Debug.Log("reverting a mesh " + instanceProp.propertyPath); }

            SerializedProperty prefabProp = prefabSo.FindProperty(instanceProp.propertyPath);
            if (prefabProp == null)
            {
                PrefabUtility.RevertPropertyOverride(instanceProp, InteractionMode.AutomatedAction);
                PrefabUtility.RecordPrefabInstancePropertyModifications(instanceComponent);
                EditorUtility.SetDirty(instanceComponent);
                continue;
            }

            float diff = instanceProp.propertyType == SerializedPropertyType.Integer ? Mathf.Abs(prefabProp.intValue - instanceProp.intValue) : Mathf.Abs(prefabProp.floatValue - instanceProp.floatValue);
            if (diff < epsilon)
            {
                Debug.LogFormat("reverting prop {0} type {1} is override {2}, diff {3}", path, type, instanceProp.prefabOverride, diff);
                PrefabUtility.RevertPropertyOverride(instanceProp, InteractionMode.AutomatedAction);
                PrefabUtility.RecordPrefabInstancePropertyModifications(instanceComponent);
                EditorUtility.SetDirty(instanceComponent);
            }
        }
    }

    public static void RevertBrokenReferenceOverrides(this Component instanceComponent)
    {
        Component prefabComp = PrefabUtility.GetCorrespondingObjectFromSource(instanceComponent);
        if (prefabComp == null)
            return;

        SerializedObject instanceSo = new SerializedObject(instanceComponent);
        SerializedObject prefabSo = new SerializedObject(prefabComp);
        SerializedProperty instanceProp = instanceSo.GetIterator();
        while (instanceProp.Next(true))
        {
            string path = instanceProp.propertyPath;
            SerializedPropertyType type = instanceProp.propertyType;
            if (!instanceProp.prefabOverride || instanceProp.isDefaultOverride || (instanceProp.propertyType != SerializedPropertyType.ObjectReference))
                continue;

            if (instanceProp.objectReferenceInstanceIDValue == 0)
            {
                PrefabUtility.RevertPropertyOverride(instanceProp, InteractionMode.AutomatedAction);
                PrefabUtility.RecordPrefabInstancePropertyModifications(instanceComponent);
                EditorUtility.SetDirty(instanceComponent);
            }
        }
    }

    public static bool HasOverridesAtPath(this Component instanceComponent, string path)
    {
        Component prefabComp = PrefabUtility.GetCorrespondingObjectFromSource(instanceComponent);
        if (prefabComp == null)
            return false;

        SerializedObject instanceSo = new SerializedObject(instanceComponent);
        //SerializedObject prefabSo = new SerializedObject(prefabComp);
        SerializedProperty instanceProp = instanceSo.FindProperty(path);
        if (instanceProp == null)
            return false;

        //if (!instanceProp.isArray)
            return instanceProp.prefabOverride;

        //else

    }

    public static void RevertOverridesAtPath(this Component instanceComponent, string path)
    {
        Component prefabComp = PrefabUtility.GetCorrespondingObjectFromSource(instanceComponent);
        if (prefabComp == null)
            return;

        SerializedObject instanceSo = new SerializedObject(instanceComponent);
        SerializedProperty prop = instanceSo.FindProperty(path);
        if (prop == null)
            return;

        PrefabUtility.RevertPropertyOverride(prop, InteractionMode.AutomatedAction);
        PrefabUtility.RecordPrefabInstancePropertyModifications(instanceComponent);
        EditorUtility.SetDirty(instanceComponent);
    }

    public static void AssignElementwise(this SplinePoint n, SplinePoint o)
    {
        n.position.x = o.position.x;
        n.position.y = o.position.y;
        n.position.z = o.position.z;
        n.tangent.x = o.tangent.x;
        n.tangent.y = o.tangent.y;
        n.tangent.z = o.tangent.z;
        n.tangent2.x = o.tangent2.x;
        n.tangent2.y = o.tangent2.y;
        n.tangent2.z = o.tangent2.z;
        n.normal.x = o.normal.x;
        n.normal.y = o.normal.y;
        n.normal.z = o.normal.z;
        n.size = o.size;
        n.type = o.type;
        n.color = o.color;
    }
}